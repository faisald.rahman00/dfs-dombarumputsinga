Project URL: https://gitlab.com/faisald.rahman00/dfs-dombarumputsinga

DFS problem - Domba-rumput-singa.

Problem:
The petani must bring domba, rumput and singa to the other side. This problem must be solved using DFS algorithm

Rule:
1. If domba and rumput are left behind, then domba will eat rumput.
2. If domba and singa are left behind, then singa will eat domba.

This project is created using Java with Apache Netbeans as IDE.

The build file is located in target/Domba-SInga-Rumput-1.0-SNAPSHOT.jar. If want to running the jar, make sure Java is installed.
