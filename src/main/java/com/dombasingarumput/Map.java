/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.dombasingarumput;

/**
 *
 * @author Alkaafi Computer
 */
public class Map {

    private int[] pos = {0,0,0,0};

    public Map(int pos0, int pos1, int pos2, int pos3) {
        this.pos[0] = pos0; //petani
        this.pos[1] = pos1; //rmput
        this.pos[2] = pos2; //domba
        this.pos[3] = pos3; //singa
    }

    public void setPos(int[] pos) {
        this.pos = pos;
    }

    public void setPosByIndex(int index, int value) {
        this.pos[index] = value;
    }

    public boolean isOccupied() {
        return this.pos[0] == 1 && this.pos[1] == 1 && this.pos[2] == 1 && this.pos[3] == 1;
    }

    public boolean isCanEat() {
        return (this.pos[0] == 0 && this.pos[1] == 1 && this.pos[2] == 1 && this.pos[3] == 0) ||
                (this.pos[0] == 0 && this.pos[1] == 0 && this.pos[2] == 1 && this.pos[3] == 1);
    }

    public int[] getPos() {
        return this.pos;
    }
}
