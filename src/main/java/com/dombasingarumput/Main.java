/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package com.dombasingarumput;

import java.util.List;
import java.util.Arrays;
import java.util.ArrayList;

/**
 *
 * @author Alkaafi Computer
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        List<Node> nodes = new ArrayList<Node>();
        Node initNode = new Node();
        initNode.showStatus();
        System.out.println("--");

        nodes.add(initNode);
        int indexParentNode = 0;
        int indexActiveNode = 0;
        Node activeNode = nodes.get(0);

        boolean isSolutionFound = false;

        while(!isSolutionFound) {
            boolean isGoToPrevParent = false;
            while(!activeNode.isEnableToMoveByRule(activeNode.getChildRule())) {
                activeNode.setChildRule(activeNode.getChildRule() + 1);
                if (activeNode.isChildRuleReachMax()) {
                    System.out.println("^^^^");
                    System.out.println("No Solution on all childnode from parent " + indexParentNode);

                    indexParentNode = activeNode.getIndexParentNode();
                    activeNode = nodes.get(indexParentNode);
                    isGoToPrevParent = true;

                    System.out.println("Back to parent " + indexParentNode);
                    System.out.println("^^^^");
                    break;
                }
            }

            if (isGoToPrevParent) continue;

            Node childNode = new Node();
            childNode.setMap(activeNode);
            childNode.setParentNode(indexParentNode);
            indexActiveNode += 1;
            childNode.setIndexNode(indexActiveNode);

            childNode.moveObject(activeNode.getChildRule());
            if (activeNode.getPetaniPosition() == 0) {
                childNode.setChildRule(3);
            } else if (activeNode.getPetaniPosition() == 1) {
                childNode.setChildRule(0);
            }
            activeNode.setChildRule(activeNode.getChildRule() + 1);

            if (childNode.isCanEat()) {
                childNode.setReason("Eat");
                childNode.setIsAcceptable(false);
                activeNode = nodes.get(childNode.getIndexParentNode());
            } else if (isNodeHasSamePosition(nodes, childNode)) {
                childNode.setReason("The map is same with any previos node");
                childNode.setIsAcceptable(false);
                activeNode = nodes.get(childNode.getIndexParentNode());
            } else if(childNode.isAllCrossed()) {
                isSolutionFound = true;
                activeNode = childNode;
            } else {
                activeNode = childNode;
                indexParentNode = indexActiveNode;
            }

            nodes.add(childNode);
            childNode.showStatus();
            if (!childNode.getIsAcceptable())
            System.out.println("**Back to " + indexParentNode + " Node" );
            System.out.println("--");
        }

        System.out.println("###");
        System.out.println("Solution is Found!");
        System.out.println("###");

        System.out.println("");
        showSolutionPath(nodes);
    }

    public static boolean isNodeHasSamePosition(List<Node> nodes, Node node) {
        for(int i=0; i<nodes.size(); i++) {
            Node temp = nodes.get(i);
            if (Arrays.equals(temp.getLeftMap().getPos(), node.getLeftMap().getPos()) && 
                Arrays.equals(temp.getRightMap().getPos(), node.getRightMap().getPos())) {
                return true;
            }
        }

        return false;
    }

    public static void showSolutionPath(List<Node> nodes) {
        int myNode = nodes.get(nodes.size() - 1).getIndexNode();
        String myRule = nodes.get(nodes.size() - 1).getMyRuleStatus();
        int prevNode = nodes.get(nodes.size() - 1).getIndexParentNode();
        String result = prevNode + " -> " + myNode;
        String resultRule = "" + myRule;
        while(prevNode > 0) {
            myRule  = nodes.get(prevNode).getMyRuleStatus();
            prevNode = nodes.get(prevNode).getIndexParentNode();
            
            result = prevNode + " -> " + result;
            resultRule = myRule + " -> " + resultRule;
        }
        resultRule = "Init -> " + resultRule;
        System.out.println("Node Path : " + result);
        System.out.println("Rule      : " + resultRule);
    }
}
