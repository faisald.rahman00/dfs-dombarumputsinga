/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.dombasingarumput;

import java.util.Arrays;

/**
 *
 * @author Alkaafi Computer
 */
public class Node {

    private Map letfMap;
    private Map rightMap;

    private int petaniPosition = 0; // indikasi posisi petani ada dimana, 0 = kiri, 1 = kanan
    private int indexNode = 0;
    private int indexParentNode = -1; // -1 artinya gka punya parent node
    private int childRule = 0; //rule yang akan digunakan child
    private int myRule = -1; //rule yang telah digunakan pada node ini
    private String reason = ""; //alasan kenapa node ini tidak acceptable

    private boolean isAcceptable = true; //jika false, artinya node gak bisa menghasilkan child

    public Node() {
        letfMap = new Map(1,1,1,1);
        rightMap = new Map(0,0,0,0);
    }

    public void moveObject(int rule) {
        myRule = rule;
        //rule berdasarkan di dokumen
        switch(rule) {
            case 0:
                this.letfMap.setPosByIndex(0, 0);
                this.letfMap.setPosByIndex(2, 0);
                this.rightMap.setPosByIndex(0, 1);
                this.rightMap.setPosByIndex(2, 1);
                this.petaniPosition = 1;
            break;
            case 1:
                this.letfMap.setPosByIndex(0, 0);
                this.letfMap.setPosByIndex(1, 0);
                this.rightMap.setPosByIndex(0, 1);
                this.rightMap.setPosByIndex(1, 1);
                this.petaniPosition = 1;
            break;
            case 2:
                this.letfMap.setPosByIndex(0, 0);
                this.letfMap.setPosByIndex(3, 0);
                this.rightMap.setPosByIndex(0, 1);
                this.rightMap.setPosByIndex(3, 1);
                this.petaniPosition = 1;
            break;  
            case 3:
                this.letfMap.setPosByIndex(0, 1);
                this.letfMap.setPosByIndex(2, 1);
                this.rightMap.setPosByIndex(0, 0);
                this.rightMap.setPosByIndex(2, 0);
                this.petaniPosition = 0;
            break;
            case 4:
                this.letfMap.setPosByIndex(0, 1);
                this.letfMap.setPosByIndex(1, 1);
                this.rightMap.setPosByIndex(0, 0);
                this.rightMap.setPosByIndex(1, 0);
                this.petaniPosition = 0;
            break;
            case 5:
                this.letfMap.setPosByIndex(0, 1);
                this.letfMap.setPosByIndex(3, 1);
                this.rightMap.setPosByIndex(0, 0);
                this.rightMap.setPosByIndex(3, 0);
                this.petaniPosition = 0;
            break;
            case 6:
                this.letfMap.setPosByIndex(0, 1);
                this.rightMap.setPosByIndex(0, 0);
                this.petaniPosition = 0;
            break;
        }
    }

    public void showStatus() {
        System.out.println("Index Node      : "+this.indexNode);
        System.out.println("Parent Node     : "+this.indexParentNode);
        System.out.println("Is Acceptable ? : "+this.isAcceptable);
        System.out.println("Reason          : "+this.reason);
        System.out.println("My Rule         : "+this.myRule);
        System.out.println("Rule Detail     : "+this.getMyRuleStatus());
        System.out.println("Petani Pos      : "+ (this.petaniPosition == 0 ? "Left" : "Right"));
        String strPos =    "Position        : ";
        int[] tempPos = this.letfMap.getPos();
        strPos += tempPos[0] + "," +  tempPos[1] + "," +  tempPos[2] + "," + tempPos[3] + " -- ";
        tempPos = this.rightMap.getPos();
        strPos += tempPos[0] + "," +  tempPos[1] + "," +  tempPos[2] + "," + tempPos[3];
        System.out.println(strPos);
    }

    public String getMyRuleStatus() {
        if (this.myRule == -1) return "";

        switch(this.myRule) {
            case 0:
                return "Petani and domba move to right";
            case 1:
                return "Petani and rumput move to right";
            case 2:
                return "Petani and singa move to right";
            case 3:
                return "Petani and domba move to left";
            case 4:
                return "Petani and rumput move to left";
            case 5:
                return "Petani and singa move to left";
            default:
                return "Petani move to left";
        }
    }

    public void setIndexNode(int indexNode) {
        this.indexNode = indexNode;
    }

    public void setParentNode(int indexNode) {
        this.indexParentNode = indexNode;
    }

    public void setMap(Node node) {
        this.letfMap.setPos(Arrays.copyOf(node.getLeftMap().getPos(), node.getLeftMap().getPos().length));
        this.rightMap.setPos(Arrays.copyOf(node.getRightMap().getPos(), node.getRightMap().getPos().length));
    }

    public void setChildRule(int index) {
        this.childRule = index;
    }

    public void setIsAcceptable(boolean isAcceptable) {
        this.isAcceptable = isAcceptable;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    //cek apakah semua object berhasil nyebrang
    public boolean isAllCrossed() {
        return this.rightMap.isOccupied();
    }

    public boolean isCanEat() {
        return this.letfMap.isCanEat() || this.rightMap.isCanEat();
    }

    public int getIndexNode() {
        return this.indexNode;
    }

    public int getIndexParentNode() {
        return this.indexParentNode;
    }

    public boolean getIsAcceptable() {
        return this.isAcceptable;
    }

    public int getPetaniPosition() {
        return petaniPosition;
    }

    public Map getLeftMap() {
        return this.letfMap;
    }

    public Map getRightMap() {
        return this.rightMap;
    }

    public int getChildRule() {
        return this.childRule;
    }

    public boolean isEnableToMoveByRule(int rule) {
        if (rule == 6) return true;

        if (this.petaniPosition == 0) {
            int[] temp = this.letfMap.getPos();
            if ((temp[1] == 1 && rule == 1) || (temp[2] == 1 && rule == 0) || (temp[3] == 1 && rule == 2)) {
                return true;
            }
        } else if (this.petaniPosition == 1) {
            int[] temp = this.rightMap.getPos();
            if ((temp[1] == 1 && rule == 4) || (temp[2] == 1 && rule == 3) || (temp[3] == 1 && rule == 5)) {
                return true;
            }
        }

        return false;
    }

    public boolean isChildRuleReachMax() {
        return (this.petaniPosition == 0 && childRule > 3) || (this.petaniPosition == 1 && childRule > 6);
    }
}
